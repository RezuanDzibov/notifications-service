class ClientDoesNotExistError(Exception):
    pass


class DistributionDoesNotExistError(Exception):
    pass
