from sqlalchemy import insert, delete, select, exists
from sqlalchemy.ext.asyncio import AsyncSession

from models import Distribution
from schemas.distribution import BaseDistributionSchema, DistributionSchema
from services.exceptions import DistributionDoesNotExistError


async def insert_distribution(
        session: AsyncSession,
        distribution_in: BaseDistributionSchema
) -> Distribution:
    statement = insert(Distribution).values(distribution_in.model_dump())
    statement = statement.returning(Distribution)
    result = await session.execute(statement)
    await session.commit()
    distribution = result.scalar()
    return distribution


async def add_distribution(
        session: AsyncSession,
        distribution_in: BaseDistributionSchema
) -> DistributionSchema:
    distribution = await insert_distribution(
        session=session,
        distribution_in=distribution_in
    )
    distribution_data = distribution.as_dict()
    return DistributionSchema(**distribution_data)


async def delete_distribution(session: AsyncSession, distribution_id: int) -> None:
    statement = exists(select(Distribution).where(Distribution.id == distribution_id)).select()
    result = await session.execute(statement)
    is_distribution_exists = result.one()[0]
    if not is_distribution_exists:
        raise DistributionDoesNotExistError
    statement = delete(Distribution).where(Distribution.id == distribution_id)
    await session.execute(statement)
    await session.commit()
