from sqlalchemy import insert, update, delete, select, exists
from sqlalchemy.ext.asyncio import AsyncSession

from models import Client
from schemas.client import BaseClientSchema, ClientSchema, UpdateClientSchema
from services.exceptions import ClientDoesNotExistError


async def insert_client(
        session: AsyncSession,
        client_in: BaseClientSchema
):
    statement = insert(Client).values(client_in.model_dump())
    statement = statement.returning(Client)
    result = await session.execute(statement)
    await session.commit()
    client = result.scalar()
    client = ClientSchema(**client.as_dict())
    return client


async def add_client(
        session: AsyncSession,
        client_in: BaseClientSchema
)-> ClientSchema:
    client = await insert_client(
        session=session,
        client_in=client_in
    )
    return client


async def update_client(
        session: AsyncSession,
        client_id: int,
        client_in: UpdateClientSchema
) -> ClientSchema:
    statement = update(Client).where(Client.id == client_id).values(client_in.model_dump(
        exclude_unset=True,
        exclude_defaults=True,
        exclude_none=True
    ))
    statement = statement.returning(Client)
    result = await session.execute(statement)
    await session.commit()
    client = result.scalar()
    client = ClientSchema(**client.as_dict())
    return client


async def delete_client(session: AsyncSession, client_id: int) -> bool:
    statement = exists(select(Client).where(Client.id == client_id)).select()
    result = await session.execute(statement)
    is_client_exists = result.scalar()
    if not is_client_exists:
        raise ClientDoesNotExistError
    statement = delete(Client).where(Client.id == client_id)
    await session.execute(statement)
    await session.commit()
