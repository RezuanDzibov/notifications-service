from fastapi import FastAPI
from starlette.responses import RedirectResponse

from settings import get_settings
from api.router import router

settings = get_settings()
app = FastAPI(title=settings.PROJECT_TITLE)
app.include_router(router)


@app.get("/", include_in_schema=False)
async def redirect_to_docs():
    return RedirectResponse("/docs")


@app.get("/healthcheck")
async def healthcheck():
    return {"status": "Ok"}
