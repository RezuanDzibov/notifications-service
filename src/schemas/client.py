import re
from typing import Optional

from pydantic import BaseModel, constr, field_validator, conint, ConfigDict
from pydantic import computed_field

from settings import get_settings

settings = get_settings()


class BaseClientSchema(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    tag: constr(max_length=35, strip_whitespace=True, to_lower=True)
    phone_number: constr(strip_whitespace=True, to_lower=True, max_length=11)
    timezone: constr(max_length=4)

    @field_validator("phone_number")
    def check_phone_number(cls, value: str) -> str:
        if re.match(settings.PHONE_NUMBER_REGEX, value):
            return value
        raise ValueError("Invalid phone number")

    @field_validator("timezone")
    def check_timezone(cls, value: str) -> str:
        if re.match(settings.TIMEZONE_POSITIVE_REGEX, value) or re.match(
                settings.TIMEZONE_NEGATIVE_REGEX, value
        ):
            return value
        raise ValueError("Invalid timezone")


class UpdateClientSchema(BaseClientSchema):
    tag: Optional[constr(max_length=35, strip_whitespace=True, to_lower=True)] = None
    phone_number: Optional[
        constr(strip_whitespace=True, to_lower=True, max_length=11)
    ] = None
    timezone: Optional[constr(max_length=4)] = None


class ClientSchema(BaseClientSchema):
    id: conint(ge=1)

    @computed_field
    @property
    def phone_operator_code(self) -> str:
        return self.phone_number[1:4]
