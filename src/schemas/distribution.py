from datetime import datetime

from pydantic import constr, conint, BaseModel, field_validator
from pydantic_core.core_schema import ValidationInfo, datetime_schema

from settings import get_settings

settings = get_settings()


class BaseDistributionSchema(BaseModel):
    start_datetime: datetime
    end_datetime: datetime
    message_text: str
    tags: list[constr(max_length=35, strip_whitespace=True, to_lower=True)]
    phone_operator_codes: list[conint(lt=1000)]

    @field_validator("start_datetime")
    @classmethod
    def validate_start_datetime(cls, value: datetime) -> datetime:
        try:
            datetime.strptime(str(value), settings.VALID_DATETIME_FORMAT)
        except ValueError:
            raise ValueError(
                "Invalid datetime format for field start_datetime. Please provide in the format YYYY-MM-DD HH:MM or YYYY-MM-DD HH:MM:SS"
            )
        return value

    @field_validator("end_datetime")
    @classmethod
    def validate_end_datetime(cls, value: datetime) -> datetime:
        try:
            datetime.strptime(str(value), settings.VALID_DATETIME_FORMAT)
        except ValueError:
            raise ValueError(
                "Invalid datetime format for field end_datetime. Please provide in the format YYYY-MM-DD HH:MM or YYYY-MM-DD HH:MM:SS"
            )
        return value

    @field_validator("end_datetime")
    @classmethod
    def validate_end_datetime_not_over_than_start_datetime(
            cls,
            value: datetime,
            info: ValidationInfo
    ) -> datetime:
        if info.data.get("start_datetime") > value:
            raise ValueError("end_datetime can't be over then end_datetime")
        return value


class DistributionSchema(BaseDistributionSchema):
    start_datetime: datetime_schema(strict=False)
    end_datetime: datetime_schema(strict=False)
    id: conint(ge=1)

    @field_validator("start_datetime")
    @classmethod
    def validate_start_datetime(cls, value: datetime) -> str:
        return str(
            datetime.strptime(str(value), settings.VALID_DATETIME_FORMAT)
        )

    @field_validator("end_datetime")
    @classmethod
    def validate_end_datetime(cls, value: datetime) -> str:
        return str(
            datetime.strptime(str(value), settings.VALID_DATETIME_FORMAT)
        )