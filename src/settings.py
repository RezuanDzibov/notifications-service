from functools import lru_cache
from pathlib import Path

from pydantic import PostgresDsn
from pydantic_settings import BaseSettings, SettingsConfigDict

BASE_DIR = Path(__file__).resolve().parent.parent


class Settings(BaseSettings):
    model_config = SettingsConfigDict(
            env_file = Path(f"{BASE_DIR}/.env"),
            extra="ignore"
    )

    PROJECT_TITLE: str

    DATABASE_ENGINE: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_PORT: str
    POSTGRES_DATABASE: str
    POSTGRES_HOST: str

    TIMEZONE_NEGATIVE_REGEX: str = r"-(?:0|(?:[1-9]|1[0-2]))$"
    TIMEZONE_POSITIVE_REGEX: str = r"\+(?:0|[1-9]|1[0-4])$"
    PHONE_NUMBER_REGEX: str = r"^7\d{10}$"

    VALID_DATETIME_FORMAT: str = "%Y-%m-%d %H:%M:%S"

    @property
    def SQLALCHEMY_DATABASE_URI(self) -> str:
        return PostgresDsn.build(
            scheme=self.DATABASE_ENGINE,
            username=self.POSTGRES_USER,
            password=self.POSTGRES_PASSWORD,
            host=self.POSTGRES_HOST,
            path=f"{self.POSTGRES_DATABASE}",
        )


@lru_cache
def get_settings() -> Settings:
    return Settings()
