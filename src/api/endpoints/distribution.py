from fastapi import APIRouter, Depends, Response
from sqlalchemy.ext.asyncio import AsyncSession

from api.dependencies import get_session
from schemas.distribution import DistributionSchema, BaseDistributionSchema
from services import distribution as services
from services.exceptions import DistributionDoesNotExistError

router = APIRouter()


@router.post("", response_model=DistributionSchema)
async def add_distribution(
        distribution_in: BaseDistributionSchema,
        session: AsyncSession = Depends(get_session)
):
    distribution = await services.add_distribution(
        session=session,
        distribution_in=distribution_in
    )
    return distribution


@router.delete("/{distribution_id}", status_code=204)
async def delete_distribution(distribution_id: int, session: AsyncSession = Depends(get_session)):
    try:
        await services.delete_distribution(session=session, distribution_id=distribution_id)
    except DistributionDoesNotExistError:
        return Response(status_code=404, content=f"Distribution with id: {distribution_id} does not exist")
    return Response(status_code=204)