from fastapi import APIRouter, Depends, Response
from pydantic import conint
from sqlalchemy.ext.asyncio import AsyncSession

from api.dependencies import get_session
from schemas.client import BaseClientSchema, ClientSchema, UpdateClientSchema
from services import client as services
from services.exceptions import ClientDoesNotExistError

router = APIRouter()

@router.post("", response_model=ClientSchema)
async def add_client(
        clint_in: BaseClientSchema,
        session: AsyncSession = Depends(get_session)
):
    client_out = await services.add_client(session=session, client_in=clint_in)
    return client_out


@router.patch("/{client_id}", response_model=ClientSchema)
async def update_client(
        client_id: conint(ge=1),
        client_in: UpdateClientSchema,
        session: AsyncSession = Depends(get_session)
):
    client_out = await services.update_client(
        session=session,
        client_id=client_id,
        client_in=client_in
    )
    return client_out


@router.delete("/{client_id}")
async def delete_client(
        client_id: conint(ge=1),
        session: AsyncSession = Depends(get_session)
):
    try:
        await services.delete_client(session=session, client_id=client_id)
    except ClientDoesNotExistError:
        return Response(
            content=f"Client with id: {client_id} doesn't exist",
            status_code=404
        )
    return Response(status_code=204)
