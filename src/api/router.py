from fastapi import APIRouter

from api.endpoints.client import router as client_router
from api.endpoints.distribution import router as distribution_router

router = APIRouter()
router.include_router(client_router, prefix="/client", tags=["client"])
router.include_router(
    distribution_router,
    prefix="/distribution",
    tags=["distribution"]
)