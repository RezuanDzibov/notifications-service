from typing import TypeVar

from sqlalchemy import inspect
from sqlalchemy.exc import NoInspectionAvailable
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column


class Base(DeclarativeBase):
    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    __name__: str

    @declared_attr
    def __tablename__(cls) -> str:
        return cls.__name__.lower()

    def as_dict(self) -> dict:
        try:
            model_data: dict = inspect(self).dict
            for key, value in model_data.items():
                if isinstance(value, list) and value:
                    serialized_items = list()
                    for item in value:
                        if isinstance(item, Base):
                            serialized_items.append(item.as_dict())
                        else:
                            serialized_items.append(item)
                    model_data[key] = serialized_items
        except NoInspectionAvailable:
            model_data = self.__dict__
        if hasattr(self, "_sa_instance_state"):
            model_data.pop("_sa_instance_state")
        return model_data


BaseModel = TypeVar("BaseModel", bound=Base)
