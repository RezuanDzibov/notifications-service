import datetime

from sqlalchemy import String, ForeignKey, Column, Integer
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import Mapped, mapped_column

from .base import Base


class Distribution(Base):
    start_datetime: Mapped[datetime.datetime] = mapped_column()
    message_text: Mapped[str] = mapped_column()
    tags: list[str] = Column(ARRAY(String))
    phone_operator_codes: list[int] = Column(ARRAY(Integer))
    end_datetime: Mapped[datetime.datetime] = mapped_column()


class Client(Base):
    phone_number: Mapped[str] = mapped_column(String(length=11))
    tag: Mapped[str] = mapped_column()
    timezone: Mapped[str] = mapped_column()

    @property
    def phone_operator_code(self) -> str:
        return self.phone_number[1:4]


class Message(Base):
    created_at: Mapped[datetime.datetime] = mapped_column()
    is_sent: Mapped[bool] = mapped_column()
    distribution_id: Mapped[int] = mapped_column(
        ForeignKey("distribution.id")
    )
    sent_to: Mapped[int] = mapped_column(ForeignKey("client.id"))
